package com.hitanshudhawan.espressoexample.five;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.hitanshudhawan.espressoexample.R;

public class FiveIntentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_five_intent);
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent();
        intent.putExtra("key", "Hello, World!");
        setResult(RESULT_OK, intent);

        super.onBackPressed();
    }
}
