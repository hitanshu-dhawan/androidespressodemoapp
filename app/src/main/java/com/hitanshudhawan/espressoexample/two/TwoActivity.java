package com.hitanshudhawan.espressoexample.two;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.hitanshudhawan.espressoexample.R;

import java.util.ArrayList;

public class TwoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two);

        ListView listView = findViewById(R.id.list_view);
        final ArrayList<String> arrayList = new ArrayList<>();
        for (int i = 1; i <= 100; i++) {
            arrayList.add("item " + i);
        }
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(TwoActivity.this, android.R.layout.simple_list_item_1, arrayList);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                arrayList.set(position,"Hello, World!");
                arrayAdapter.notifyDataSetChanged();
            }
        });

    }
}
