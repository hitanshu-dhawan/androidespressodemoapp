package com.hitanshudhawan.espressoexample.three;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.hitanshudhawan.espressoexample.R;

import java.util.ArrayList;
import java.util.List;

public class ThreeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_three);

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        List<String> list = new ArrayList<>();
        for (int i = 1; i <= 100; i++) {
            list.add("item " + i);
        }
        Adapter adapter = new Adapter(this,list);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
    }
}
