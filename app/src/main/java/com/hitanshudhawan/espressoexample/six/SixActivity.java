package com.hitanshudhawan.espressoexample.six;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.IdlingResource;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.hitanshudhawan.espressoexample.R;

import org.json.JSONException;
import org.json.JSONObject;

public class SixActivity extends AppCompatActivity {

    @Nullable
    private SimpleIdlingResource mIdlingResource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_six);

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new MyAsyncTask(new MyAsyncTask.OnDownloadListener() {
                    @Override
                    public void onDownloaded(JSONObject jsonObject) {
                        try {
                            if (jsonObject == null) return;
                            String quote = jsonObject.getJSONObject("contents")
                                    .getJSONArray("quotes").getJSONObject(0)
                                    .getString("quote");
                            String author = jsonObject.getJSONObject("contents")
                                    .getJSONArray("quotes").getJSONObject(0)
                                    .getString("author");

                            ((TextView)findViewById(R.id.text_view)).setText(quote + "\n - " + author);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },mIdlingResource).execute();

            }
        });
    }

    @VisibleForTesting
    @NonNull
    public IdlingResource getIdlingResource() {
        if (mIdlingResource == null) {
            mIdlingResource = new SimpleIdlingResource();
        }
        return mIdlingResource;
    }
}
