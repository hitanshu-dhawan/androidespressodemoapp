package com.hitanshudhawan.espressoexample.six;

import android.os.AsyncTask;
import android.support.annotation.Nullable;

import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by hitanshu on 15/4/18.
 */

/*
 * Remember that if idle is false there are pending operations in the background and any testing operations should be paused.
 * If idle is true all is clear and testing operations can continue.
 */

public class MyAsyncTask extends AsyncTask<Void, Void, JSONObject> {

    OnDownloadListener onDownloadListener;

    @Nullable
    SimpleIdlingResource mIdlingResource;

    public MyAsyncTask(OnDownloadListener onDownloadListener, @Nullable SimpleIdlingResource idlingResource) {
        this.onDownloadListener = onDownloadListener;
        this.mIdlingResource = idlingResource;
    }

    @Override
    protected JSONObject doInBackground(Void... voids) {

        if (mIdlingResource != null) {
            mIdlingResource.setIdleState(false);
        }

        try {
            URL url = new URL("https://quotes.rest/qod");
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.connect();

            if (httpURLConnection.getResponseCode() != 200) return null;

            InputStream inputStream = httpURLConnection.getInputStream();
            Scanner scanner = new Scanner(inputStream);
            String jsonString = "";
            while (scanner.hasNext()) {
                jsonString += scanner.nextLine();
            }

            return new JSONObject(jsonString);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        onDownloadListener.onDownloaded(jsonObject);
        if (mIdlingResource != null) {
            mIdlingResource.setIdleState(true);
        }
    }

    public interface OnDownloadListener {
        void onDownloaded(JSONObject jsonObject);
    }
}
