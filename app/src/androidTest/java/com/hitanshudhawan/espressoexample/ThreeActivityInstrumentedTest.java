package com.hitanshudhawan.espressoexample;

import android.support.annotation.NonNull;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.test.rule.ActivityTestRule;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.hitanshudhawan.espressoexample.three.ThreeActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static android.support.test.internal.util.Checks.checkNotNull;
import static org.hamcrest.Matchers.anything;

/**
 * Created by hitanshu on 31/3/18.
 */

public class ThreeActivityInstrumentedTest {

    @Rule
    public ActivityTestRule<ThreeActivity> mThreeActivityActivityTestRule
            = new ActivityTestRule<>(ThreeActivity.class);

    @Test
    public void checkOnItemClickListener() {
        onView(withId(R.id.recycler_view)).perform(RecyclerViewActions.actionOnItemAtPosition(49, click()));

        // if ViewHolder is a View
        onView(withId(R.id.recycler_view)).check(matches(atPosition(49, withText("Hello, World!"))));
        // if ViewHolder is a ViewGroup
        // onView(withId(R.id.recycler_view)).check(matches(atPosition(49, hasDescendant(withText("Hello, World!")))));
    }

    private static Matcher<View> atPosition(final int position, @NonNull final Matcher<View> itemMatcher) {
        checkNotNull(itemMatcher);
        return new BoundedMatcher<View, RecyclerView>(RecyclerView.class) {
            @Override
            public void describeTo(Description description) {
                description.appendText("has item at position " + position + ": ");
                itemMatcher.describeTo(description);
            }

            @Override
            protected boolean matchesSafely(final RecyclerView view) {
                RecyclerView.ViewHolder viewHolder = view.findViewHolderForAdapterPosition(position);
                return viewHolder != null && itemMatcher.matches(viewHolder.itemView);
            }
        };
    }

}
