package com.hitanshudhawan.espressoexample;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.hitanshudhawan.espressoexample.five.FiveActivity;
import com.hitanshudhawan.espressoexample.five.FiveIntentActivity;
import com.hitanshudhawan.espressoexample.four.FourActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.ComponentNameMatchers.hasShortClassName;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasAction;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasData;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasType;
import static android.support.test.espresso.intent.matcher.IntentMatchers.isInternal;
import static android.support.test.espresso.intent.matcher.IntentMatchers.toPackage;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;

/**
 * Created by hitanshu on 2/4/18.
 */

@RunWith(AndroidJUnit4.class)
public class FiveActivityInstrumentedTest {

    @Rule
    public IntentsTestRule<FiveActivity> mFiveActivityIntentsTestRule
            = new IntentsTestRule<>(FiveActivity.class);

    @Before
    public void stubContactIntent() {
        Intent intent = new Intent();
        intent.putExtra("key","Hello, World from Testing!");
        Instrumentation.ActivityResult result = new Instrumentation.ActivityResult(Activity.RESULT_OK, intent);

        intending(hasComponent(FiveIntentActivity.class.getName()))
                .respondWith(result);
    }

    @Test
    public void intentStubbing() {
        onView(withId(R.id.button)).perform(click());
        onView(withId(R.id.text_view)).check(matches(withText("Hello, World from Testing!")));
    }

}
