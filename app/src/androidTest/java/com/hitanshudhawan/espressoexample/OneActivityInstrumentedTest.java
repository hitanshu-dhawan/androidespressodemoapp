package com.hitanshudhawan.espressoexample;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.hitanshudhawan.espressoexample.one.OneActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by hitanshu on 29/3/18.
 */

@RunWith(AndroidJUnit4.class)
public class OneActivityInstrumentedTest {

    @Rule
    public ActivityTestRule<OneActivity> mOneActivityActivityTestRule
            = new ActivityTestRule<>(OneActivity.class);

    @Test
    public void checkIfTextChanges() {
        onView(withId(R.id.button)).perform(click());
        onView(withId(R.id.text_view)).check(matches(withText("Hello, World!")));
    }

}
