package com.hitanshudhawan.espressoexample;

import android.content.Intent;
import android.net.Uri;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.hitanshudhawan.espressoexample.four.FourActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasAction;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasData;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;

/**
 * Created by hitanshu on 2/4/18.
 */

@RunWith(AndroidJUnit4.class)
public class FourActivityInstrumentedTest {

    @Rule
    public IntentsTestRule<FourActivity> mFourActivityIntentsTestRule
            = new IntentsTestRule<>(FourActivity.class);

    @Test
    public void intentVerification() {

        onView(withText("hitanshudhawan")).perform(click());

        intended(allOf(
                hasAction(Intent.ACTION_VIEW),
                hasData(Uri.parse("http://hitanshudhawan.com"))));
    }

}
