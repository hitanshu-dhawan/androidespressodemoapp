package com.hitanshudhawan.espressoexample;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.hitanshudhawan.espressoexample.two.TwoActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.anything;

/**
 * Created by hitanshu on 31/3/18.
 */

@RunWith(AndroidJUnit4.class)
public class TwoActivityInstrumentedTest {

    @Rule
    public ActivityTestRule<TwoActivity> mTwoActivityActivityTestRule
            = new ActivityTestRule<>(TwoActivity.class);

    @Test
    public void checkOnItemClickListener() {
        onData(anything()).inAdapterView(withId(R.id.list_view)).atPosition(49).perform(click());
        onData(anything()).inAdapterView(withId(R.id.list_view)).atPosition(49).check(matches(withText("Hello, World!")));
    }

}
