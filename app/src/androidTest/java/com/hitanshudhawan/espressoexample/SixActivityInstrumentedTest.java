package com.hitanshudhawan.espressoexample;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.IdlingResource;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.hitanshudhawan.espressoexample.six.SixActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.registerIdlingResources;
import static android.support.test.espresso.Espresso.unregisterIdlingResources;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by hitanshu on 15/4/18.
 */

@RunWith(AndroidJUnit4.class)
public class SixActivityInstrumentedTest {

    @Rule
    public ActivityTestRule<SixActivity> mActivityRule = new ActivityTestRule<>(
            SixActivity.class);

    private IdlingResource mIdlingResource;

    @Before
    public void registerIdlingResource() {
        mIdlingResource = mActivityRule.getActivity().getIdlingResource();
        //registerIdlingResources(mIdlingResource);
        IdlingRegistry idlingRegistry = IdlingRegistry.getInstance();
        idlingRegistry.register(mIdlingResource);
    }

    @Test
    public void checkAsyncTaskLoaded() {
        onView(withId(R.id.button)).perform(click());
        onView(withId(R.id.text_view)).check(matches(withText("Do not wait to strike till the iron is hot; but make it hot by striking...."
                + "\n - " +
                "William B. Sprague")));
    }

    @After
    public void unregisterIdlingResource() {
        if (mIdlingResource != null) {
            //unregisterIdlingResources(mIdlingResource);
            IdlingRegistry idlingRegistry = IdlingRegistry.getInstance();
            idlingRegistry.unregister(mIdlingResource);
        }
    }
}
