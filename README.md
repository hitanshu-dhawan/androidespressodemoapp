https://in.udacity.com/course/advanced-android-app-development--ud855

Docs & Articles

https://medium.com/google-developers/adapterviews-and-espresso-f4172aa853cf
<br>
https://developer.android.com/training/testing/espresso/lists.html
<br>
http://blog.xebia.com/android-custom-matchers-in-espresso/
<br>
https://developer.android.com/training/testing/espresso/intents.html
<br>
https://developer.android.com/training/testing/espresso/idling-resource.html

Code

https://github.com/googlesamples/android-testing/tree/master/ui/espresso
